package com.example.testeeclipse;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateFormat;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.testeeclipse.entidade.Pessoa;
import com.example.testeeclipse.repository.PessoaRepository;
import android.text.format.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class ListaPessoaActivity extends AppCompatActivity {

    private ListView lstPessoas;

    private List<Pessoa> listaPessoas;

    private PessoaRepository repository;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_pessoa);

        getSupportActionBar().setTitle("Lista de Pessoas");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        repository = new PessoaRepository(this);

        lstPessoas = (ListView) findViewById(R.id.lstPessoas);

        listaPessoas = repository.listarPessoas();

        List<String> valores = new ArrayList<String>();
        for (Pessoa p : listaPessoas) {
            valores.add(p.getNome());
        }

        ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, valores);
        lstPessoas.setAdapter(adapter);

        lstPessoas.setOnItemClickListener(cliclkListenerPessoas);

    }

private AdapterView.OnItemClickListener cliclkListenerPessoas = new AdapterView.OnItemClickListener() {
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        Pessoa pessoa = listaPessoas.get(position);

        DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyy");

        StringBuilder info = new StringBuilder();
        info.append("Nome : "+pessoa.getNome());
        info.append("\nEndereço : "+pessoa.getEndereco());
        info.append("\nCPF/CNPJ : "+pessoa.getEndereco());
        info.append("\nSexo : "+pessoa.getSexo().getDescricao());
        info.append("\nProfissão : "+pessoa.getProfissao().getDescricao());
        info.append("\nDt. de Nascimento : "+  pessoa.getDtNasc().g);


    }
};


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                finish();
                break;
        }
        return true;
    }

    public void addNewPessoa(View view){
        Intent i = new Intent(this,PessoaActivity.class);
        startActivity(i);
    }
}
