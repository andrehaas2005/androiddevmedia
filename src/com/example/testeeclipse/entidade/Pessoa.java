package com.example.testeeclipse.entidade;

import java.util.Date;

/**
 * Created by Diogo Souza on 23/11/2015.
 */
public class Pessoa {

    private int idPessoa;

    private String nome;

    private String endereco;

    private String cpfCpnj;

    private TipoPessoa tipoPessoa;

    private Sexo sexo;

    private Profissao profissao;

    private Date dtNasc;

    public int getIdPessoa() {
        return idPessoa;
    }

    public void setIdPessoa(int idPessoa) {
        this.idPessoa = idPessoa;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getEndereco() {
        return endereco;
    }

    public void setEndereco(String endereco) {
        this.endereco = endereco;
    }

    public String getCpfCpnj() {
        return cpfCpnj;
    }

    public void setCpfCpnj(String cpfCpnj) {
        this.cpfCpnj = cpfCpnj;
    }

    public TipoPessoa getTipoPessoa() {
        return tipoPessoa;
    }

    public void setTipoPessoa(TipoPessoa tipoPessoa) {
        this.tipoPessoa = tipoPessoa;
    }

    public Sexo getSexo() {
        return sexo;
    }

    public void setSexo(Sexo sexo) {
        this.sexo = sexo;
    }

    public Profissao getProfissao() {
        return profissao;
    }

    public void setProfissao(Profissao profissao) {
        this.profissao = profissao;
    }

    public Date getDtNasc() {
        return dtNasc;
    }

    public void setDtNasc(Date dtNasc) {
        this.dtNasc = dtNasc;
    }

    @Override
    public String toString() {
        return "Pessoa{" +
                "nome='" + nome + '\'' +
                ", endereco='" + endereco + '\'' +
                ", cpfCpnj='" + cpfCpnj + '\'' +
                ", tipoPessoa=" + tipoPessoa +
                ", sexo=" + sexo +
                ", profissao=" + profissao +
                ", dtNasc=" + dtNasc +
                '}';
    }
}
