package com.example.testeeclipse.entidade;

/**
 * Created by Diogo Souza on 23/11/2015.
 */
public enum Sexo {

    MASCULINO("Masculino"), FEMININO("Feminino");


    private Sexo(String descricao) {
        this.descricao = descricao;
    }

    public String getDescricao() {
        return descricao;
    }

    private String descricao;

    public static Sexo getSexo(int pos) {
        for (Sexo sexo : Sexo.values()) {
            if (sexo.ordinal() == pos) {
                return sexo;
            }
        }
        return null;
    }

}
