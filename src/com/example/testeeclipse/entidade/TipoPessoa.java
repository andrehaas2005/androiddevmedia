package com.example.testeeclipse.entidade;

/**
 * Created by Diogo Souza on 23/11/2015.
 */
public enum TipoPessoa {

    FISICA, JURIDICA

}
