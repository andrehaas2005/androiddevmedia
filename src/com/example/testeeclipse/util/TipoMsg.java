package com.example.testeeclipse.util;

/**
 * Created by Diogo Souza on 08/10/2015.
 */
public enum TipoMsg {

    ERRO, INFO, SUCESSO, ALERTA

}
