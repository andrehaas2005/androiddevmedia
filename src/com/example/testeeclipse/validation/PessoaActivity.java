package com.example.testeeclipse.validation;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.support.annotation.IdRes;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextWatcher;
import android.text.format.DateFormat;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.testeeclipse.R;
import com.example.testeeclipse.entidade.Pessoa;
import com.example.testeeclipse.entidade.Profissao;
import com.example.testeeclipse.entidade.Sexo;
import com.example.testeeclipse.entidade.TipoPessoa;
import com.example.testeeclipse.repository.PessoaRepository;
import com.example.testeeclipse.util.Mask;
import com.example.testeeclipse.util.Util;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import fragment.DatePickerFragment;

public class PessoaActivity extends AppCompatActivity {

    private Spinner spnProfissao;
    private EditText edtCpfCnpj, edtNasc, edtNome, edtEndereco;
    private TextView txtCpfCnpj;
    private RadioGroup rbgCpfCnpj, rbgSexo;
    private RadioButton rbtMasc, rbtFem, rbtCpf, rbtCnpj;
    private int cpfCnpjSelecionado;

    private PessoaRepository pessoaRepository;

    private TextWatcher cpfMask, cnpjMask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pessoa);

        pessoaRepository = new PessoaRepository(PessoaActivity.this);

        spnProfissao = (Spinner) findViewById(R.id.spnProfissao);

        edtCpfCnpj = (EditText) findViewById(R.id.edtCpfCnpj);
        edtNasc = (EditText) findViewById(R.id.edtNasc);
        edtNome = (EditText) findViewById(R.id.edtNome);
        edtEndereco = (EditText) findViewById(R.id.edtEndereco);

        txtCpfCnpj = (TextView) findViewById(R.id.txtCpfCnpj);

        rbgCpfCnpj = (RadioGroup) findViewById(R.id.rbgCpfCnpj);
        rbgSexo = (RadioGroup) findViewById(R.id.rbgSexo);

        rbtCnpj = (RadioButton) findViewById(R.id.rbtCnpj);
        rbtCpf = (RadioButton) findViewById(R.id.rbtCpf);
        rbtMasc = (RadioButton) findViewById(R.id.rbtMasc);
        rbtFem = (RadioButton) findViewById(R.id.rbtFem);

        cpfMask = Mask.insert("###.###.###-##", edtCpfCnpj);
        edtCpfCnpj.addTextChangedListener(cpfMask);
        cnpjMask = Mask.insert("##.###.###/####-##", edtCpfCnpj);

        rbgCpfCnpj.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, @IdRes int checkedId) {

                edtCpfCnpj.setText("");
                edtCpfCnpj.findFocus();

                cpfCnpjSelecionado = group.getCheckedRadioButtonId();
                if (cpfCnpjSelecionado == rbtCpf.getId()) {
                    edtCpfCnpj.removeTextChangedListener(cnpjMask);
                    edtCpfCnpj.addTextChangedListener(cpfMask);
                    txtCpfCnpj.setText("CPF:");

                } else {
                    edtCpfCnpj.removeTextChangedListener(cpfMask);
                    edtCpfCnpj.addTextChangedListener(cnpjMask);
                    txtCpfCnpj.setText("CNPJ:");
                }
            }
        });


        edtCpfCnpj.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (rbgCpfCnpj.getCheckedRadioButtonId() == rbtCpf.getId()) {
                    if (edtCpfCnpj.getText().length() < 14) {
                        edtCpfCnpj.setText("");
                    }
                } else {
                    if (edtCpfCnpj.getText().length() < 18) {
                        edtCpfCnpj.setText("");
                    }
                }
            }
        });


        initProfissoes();


    }

    public void setData(View view) {

        DatePickerFragment datePickerFragment = new DatePickerFragment();
        Bundle bundle = new Bundle();

        Calendar cal = Calendar.getInstance();

        bundle.putInt("dia", cal.get(Calendar.DAY_OF_MONTH));
        bundle.putInt("mes", cal.get(Calendar.MONTH));
        bundle.putInt("ano", cal.get(Calendar.YEAR));

        datePickerFragment.setArguments(bundle);

        datePickerFragment.setDateListener(dateLister);

        datePickerFragment.show(getFragmentManager(), "Data Nasc.");

    }

//    @Override
//    protected Dialog onCreateDialog(int id) {
//        if(id == 999){
//            Calendar c = Calendar.getInstance();
//            return new  DatePickerDialog(this,dateLister,c.get(Calendar.YEAR),c.get(Calendar.MONTH),c.get(Calendar.DAY_OF_MONTH));
//        }
//
//
//        return null;
//    }

    private DatePickerDialog.OnDateSetListener dateLister = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

            edtNasc.setText(dayOfMonth + "/" + (monthOfYear + 1) + "/" + year);

            Util.showMsgToast(PessoaActivity.this, "Anos: " + year + " Mes :" + (monthOfYear + 1) + " Dia : " + dayOfMonth);
        }
    };

    public void enviarPessoa(View view) {
        Pessoa p = montarPessoa();
        if (!validarPessoa(p)) {
            pessoaRepository.salvarPessoa(p);
        }
    }


    private boolean validarPessoa(Pessoa pessoa) {

        boolean erro = false;

        if (pessoa.getNome() == null || "".equals(pessoa.getNome())) {
            erro = true;
            edtNome.setError("Campo Nome obrigatorio");
        }
        if (pessoa.getEndereco() == null || "".equals(pessoa.getEndereco())) {
            erro = true;
            edtEndereco.setError("Campo Endereço obrigatorio");
        }
        if (pessoa.getCpfCpnj() == null || "".equals(pessoa.getCpfCpnj())) {
            erro = true;
            switch (rbgCpfCnpj.getCheckedRadioButtonId()) {
                case R.id.rbtCpf:

                    edtCpfCnpj.setError("Campo CPF obrigatorio");
                    break;
                case R.id.rbtCnpj:
                    edtCpfCnpj.setError("Campo CNPL obrigatorio");
                    break;
            }


        } else {
            switch (rbgCpfCnpj.getCheckedRadioButtonId()) {
                case R.id.rbtCpf:
                    if (edtCpfCnpj.getText().length() < 14) {
                        erro = true;
                        edtCpfCnpj.setError("Campo CPF de ter 11 caracteres !");
                    }

                    break;
                case R.id.rbtCnpj:
                    if (edtCpfCnpj.getText().length() < 18) {
                        erro = true;
                        edtCpfCnpj.setError("Campo CNPF de ter 14 caracteres !");
                    }
                    break;
            }

            if (pessoa.getDtNasc() == null) {
                erro = true;
                edtNasc.setError("Campo Data Nascimento obrigatorio");
            }

        }

        return erro;
    }

    private void initProfissoes() {

        ArrayList<String> profissoes = new ArrayList<>();
        for (Profissao p : Profissao.values()) {
            profissoes.add(p.getDescricao());
        }
        ArrayAdapter adapter = new ArrayAdapter(PessoaActivity.this, android.R.layout.simple_spinner_item, profissoes);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spnProfissao.setAdapter(adapter);
    }

    private Pessoa montarPessoa() {
        Pessoa pessoa = new Pessoa();
        pessoa.setNome(edtNome.getText().toString());
        pessoa.setEndereco(edtEndereco.getText().toString());
        pessoa.setCpfCpnj(edtCpfCnpj.getText().toString());
        switch (rbgCpfCnpj.getCheckedRadioButtonId()) {
            case R.id.rbtCpf:
                pessoa.setTipoPessoa(TipoPessoa.FISICA);
                break;
            case R.id.rbtCnpj:
                pessoa.setTipoPessoa(TipoPessoa.JURIDICA);
                break;
        }
        switch (rbgSexo.getCheckedRadioButtonId()) {
            case R.id.rbtMasc:
                pessoa.setSexo(Sexo.MASCULINO);
                break;
            case R.id.rbtFem:
                pessoa.setSexo(Sexo.FEMININO);
                break;
        }


        pessoa.setProfissao(Profissao.getProfissao(spnProfissao.getSelectedItemPosition()));

        SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {
            Date nasc = dateFormat.parse(edtNasc.getText().toString());
            pessoa.setDtNasc(nasc);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return pessoa;

    }

}
