package fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.os.Bundle;

/**
 * Created by andre.haas on 19/05/2017.
 */

public class DatePickerFragment extends DialogFragment {

    private int dia, mes, ano;

    private DatePickerDialog.OnDateSetListener listener;

    public DatePickerFragment() {
    }

    public void setDateListener(DatePickerDialog.OnDateSetListener listener) {
        this.listener = listener;
    }

    @Override
    public void setArguments(Bundle args) {
        super.setArguments(args);
        ano = args.getInt("ano");
        mes = args.getInt("mes");
        dia = args.getInt("dia");
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        return new DatePickerDialog(getActivity(),listener,ano,mes,dia);
    }
}
